import { IconePesquisa } from "./Icones";

export default function BarradePesquisa(){
    return(
        <div className="flex flex-grow ">
        <input className="flex-grow focus: rounded-md mb-5 mt-2 outline-none bg-gray-100 shadow-2xl" />
        <button className=" mr-3 ml-1 mt-2 mb-5">
        {IconePesquisa}
        </button>
        </div>  
        
    )
}