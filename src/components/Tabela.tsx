import { useEffect, useState } from "react"
import Departamento from "../core/Departamento"
import DepartamentoRepositorio from "../core/Repositorio"
import { ColecaoDepartamento } from "../pages/api/departamento/ColecaoDepartamento"
import BarradePesquisa from "./BarradePesquisa"
import Botao from "./Botao"
import { IconeAtivado, IconeDesativado, IconeEdicao, IconeInativar, IconeLixo } from "./Icones"
import ModalCadastro from "./ModalCadastro"
import ModalEditar from "./ModalEditar"
import ModalExcluir from "./ModalExcluir"
import ModalInativado from "./ModalInativar"


interface TabelaProps {
    departamentos: Departamento[]
    departamentoEditado?: (departamento: Departamento) => void
    departamentoExcluido?: (departamento: Departamento) => void
    departamentoInativado?: (departamento: Departamento) => void
    deleteDpt?: (departamento: Departamento) => void
    inativarDpt?:(departamento:Departamento) =>void
    obterTodos?: () => []
}



export default function Tabela(props: TabelaProps) {

    const exibirAcoes = props.departamentoExcluido || props.departamentoEditado || props.departamentoInativado
    const [openModal, setOpenModal] = useState(false)
    const [abrirModal, setAbrirModal] = useState(false)
    const [abrirEdicao, setAbrirEdicao] = useState(false)
    const [abrirCadastro, setAbrirCadastro] = useState(false)
    const repo: DepartamentoRepositorio = new ColecaoDepartamento()
    const [departamentos, setDepartamentos] = useState<Departamento[]>([])
    const [departamento, setDepartamento] = useState<Departamento>(Departamento.vazio())
    const [mudarIcone, setMudarIcone] = useState(false)

    useEffect(() => {
        obterTodos()
    }, [])

    function obterTodos() {
        repo.obterTodos().then(departamentos => {
            setDepartamentos(departamentos)
        })
    }

    function deleteDpt(departamento: Departamento) {
        repo.excluir(departamento)
        obterTodos()
    }
    function inativarDpt(departamento:Departamento){
      repo.inativar(departamento)
      obterTodos()
        }
    function salvarDpt(departamento: Departamento) {
        repo.salvar(departamento)
        obterTodos()
        console.log(departamento)
        }
    function mudar(){
        return(
            <div 
            onClick={()=> setMudarIcone(!mudarIcone)}
            className="flex justify-end items-end bg-green-600 rounded-full"
            >
            </div>
        )
    }
        

    function renderizarCabecalho() {
        return (
            <tr className="mb-3 shadow-2xl">
                <th className="mb-3 shadow-2x1">Nome do departamento</th>
                <th >Ações</th>
            </tr>
        )
    }

    function renderizarDados() {
        return departamentos && departamentos?.map((departamento, i) => {
            return (
                <tr key={departamento.id}
                    className={`${i % 2 === 0 ? 'bg-yellow-100' : 'bg-yellow-200'} shadow-2xl`}>
                    <td>{departamento.name}</td>
                    {renderizarAcoes(departamento)}
                </tr>
            )
        })
    }

    function renderizarAcoes(departamento: Departamento) {
        return (
            <td className="flex justify-center items-center">
                {props.departamentoEditado ? (
                    <button onClick={() => setAbrirEdicao(true)} className={`
                     flex justify-center items-center
                     text-blue-800 rounded-full p-2 m1
                     
                     `}>{IconeEdicao}</button>
                ) : false}
                <button onClick={() => setOpenModal(true)} className={`
                     flex justify-center items-center
                     text-blue-800 rounded-full p-2 m1
                     
                    `}>{IconeLixo}</button>
                <button onClick={() => setAbrirModal(true)} className={`
                    flex justify-center items-center
                    text-blue-800 rounded-full p-2 m1 cursor-pointer
                `}>{IconeInativar}</button>
                {openModal && <ModalExcluir closeModal={setOpenModal} excluirDepartamento={deleteDpt} obterTodos={renderizarDados} departamento={departamento} />}
                {abrirModal && <ModalInativado fecharModal={setAbrirModal} inativarDepartamento={inativarDpt} obterTodos={renderizarDados} departamento={departamento} mudarIcone={mudar}/>}
                {abrirEdicao && <ModalEditar fecharEdicao={setAbrirEdicao} departamento={departamento} salvarDepartamento={salvarDpt} obterTodos={renderizarDados}/>}
             </td>
        )
    }
    return (
        <div>
        <div className="flex justify-end">
        <BarradePesquisa></BarradePesquisa>
        <Botao className="mb-4 mr-3 shadow-2x1" onClick={()=>setAbrirCadastro(true)}> + Novo Departamento</Botao>
        {abrirCadastro && <ModalCadastro fecharCadastro={setAbrirCadastro} departamento={departamento} cadastrarDepartamento={salvarDpt} obterTodos={renderizarDados} />}
        </div>
        <table className="w-full rounded-xl overflow-hidden shadow-xl">
            <thead className={`
            text-black
            bg-yellow-400 
            `}>
                {renderizarCabecalho()}
                <hr />
            </thead>
            <tbody>
                {renderizarDados()}
            </tbody>
        </table>
        </div>
    )
}
