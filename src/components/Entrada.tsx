interface EntradaProps {
    tipo?:'text' | 'number'
    texto: string
    valor: any
    somenteLeitura?:boolean
    valorMudou?: (valor:any)=>void
}


export default function Entrada(props: EntradaProps){
    return(
        <div className="flex flex-col" >
          <label className="mb-4">
            {props.texto}
          </label>
          <input placeholder="Campo Obrigatório" type={props.tipo ?? 'text'}
          value={props.valor}
          onChange={e=>props.valorMudou?.(e.target.value)}
          className={`
          rounded-md
          focus:outline-none
          bg-white
          px-16 py-1 mb-2 
          `}
          />
        </div>
    )
}