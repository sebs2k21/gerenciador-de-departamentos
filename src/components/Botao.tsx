interface BotaoProps{
    cor?: 'blue' | 'gray'
    children: any
    className?: string
    onClick?:() => void
}

export default function Botao(props: BotaoProps){
    const cor = props.cor ?? 'blue'
    return(
        <button onClick={props.onClick} className={`
       ${cor} bg-blue-900
        text-white px-2 py-2 rounded-md
        mb-4 m-2
        ${props.className}
        `}>
            {props.children}
        </button>
    )
}