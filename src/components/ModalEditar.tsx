import React, { useState } from "react"
import Departamento from "../core/Departamento"
import Botao from "./Botao"
import Entrada from "./Entrada"


    const ModalEditar = ({fecharEdicao, departamento, salvarDepartamento, obterTodos})=>{
    const id = departamento?.id
    const [nome, setNome] = useState(departamento?.name ?? '')
    const [vinculo, setVinculo] = useState(departamento?.vinculo ?? '')
    const editarVinculo = e => {
    e.preventDefault()
    }
    return(
      <div className="fixed inset-0 z-50">
        <div className="flex h-screen justify-center items-center">
          <div className="flex-col justify-center bg-gray-100 rounded px-8 py-5 mb-24 shadow-xl">
            <div className="flex justify-center">
                <div className="mb-2 ">

            <Entrada 
            texto=" * Nome do Departamento"
            valor={nome} 
            valorMudou={setNome}
            />
            <h1 className="mt-2">* Vinculo</h1>
            <form onSubmit={editarVinculo}>
            <select name="Entidade" value={vinculo} onChange={e=>setVinculo(e.target.value)} className="flex flex-col mt-3 w-full border-2 rounded-md bg-white">
                <option value="Entidade 1">Entidade 1</option>
                <option value="Entidade 2">Entidade 2</option>
                <option value="Entidade 3">Entidade 3</option>
                <option value="Entidade 4">Entidade 4</option>
                <option value="Entidade 5">Entidade 5</option>
                <option value="Entidade 6">Entidade 6</option>
                <option value="Entidade 7">Entidade 7</option>
                <option value="Entidade 8">Entidade 8</option>
                <option value="Entidade 9">Entidade 9</option>
                <option value="Entidade 10">Entidade 10</option>
            </select>
            </form>

            <div className="mt-2">
                <Botao className="mr-5 justify-end" 
                onClick={()=>{
                salvarDepartamento?.(new Departamento(nome, vinculo,true, id))
                fecharEdicao()
                obterTodos()
                }}>
                {id ? 'Cadastrar' : 'Salvar'}
                </Botao>
                <Botao onClick={()=>fecharEdicao()}>
                    Voltar
                </Botao>
            </div>
            </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
    export default ModalEditar 