import { IconeBox, IconeCash, IconeConfig, IconeExit, IconeGlobo, IconeGrupo, IconePredio } from "./Icones";

export default function MenuLateral(){

    return( <div className="flex flex-col justify-start bg-blue-900 rounded-r-xl">
            <button className="text-white ml-4 mb-5 mt-2 mr-4">{IconeGlobo}</button>
            <button className="text-white ml-4 mb-3 mr-4">{IconeGrupo}</button>
            <button className="text-white ml-4 mb-3 mr-4">{IconePredio}</button>
            <button className="text-white ml-4 mb-3 mr-4">{IconeBox}</button>
            <button className="text-white ml-4 mb-3 mr-4">{IconeCash}</button>
            <button className="text-white ml-4 mb-3 mr-4">{IconeConfig}</button>
            <button className="text-white ml-4 mb-3 mr-4 mt-96">{IconeExit}</button>
        </div>
    )
}