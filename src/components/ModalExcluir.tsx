import React from "react";

const ModalExcluir = ({ closeModal, excluirDepartamento, obterTodos, departamento}) => {


  return (
    <div className="fixed inset-0 z-50">
      <div className="flex h-screen justify-center items-center">
        <div className="flex-col justify-center bg-gray-100 rounded px-8 py-5 mb-24 shadow-xl">
          <div className="flex m-5">Deseja mesmo excluir esse departamento?</div>
          <div className="flex justify-center">
            <button onClick={() => closeModal(false)}
              className="rounded px-3 py-3 mt-2 flex justify-end bg-gray-300 border-2 border-blue-800 text-blue-800">
              Cancelar</button>
            <button
              className="flex justify-end rounded px-3 py-3 ml-24 mt-2 bg-red-700 text-white"
              onClick={() => {
                excluirDepartamento(departamento)
                closeModal()
                obterTodos()
              }}>
              Excluir</button>
          </div>
        </div>
      </div>
    </div>
  )
}
export default ModalExcluir;

