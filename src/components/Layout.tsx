import Titulo from "./Titulo"

interface LayoutProps {
    titulo: string
    children: any
}

export default function Layout (props: LayoutProps) {
    return(
        <div className={`
       flex flex-col w-screen  text-black 
        `}>
            <Titulo>{props.titulo}</Titulo>
            <div className=" pl-8 mr-3">
             {props.children}
            </div>
        </div>
    )
}