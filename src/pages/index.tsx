import { useEffect, useState } from 'react'

import Layout from '../components/Layout';
import MenuLateral from '../components/MenuLateral';
import ModalCadastro from '../components/ModalCadastro';
import Tabela from '../components/Tabela';
import Departamento from '../core/Departamento';
import DepartamentoRepositorio from '../core/Repositorio';
import { ColecaoDepartamento } from './api/departamento/ColecaoDepartamento';

export default function Home() {

  const repo: DepartamentoRepositorio = new ColecaoDepartamento()
  const [departamento, setDepartamento] = useState<Departamento>(Departamento.vazio())
  const [departamentos, setDepartamentos] = useState<Departamento[]>([])
  const [visivel, setVisivel] = useState<'tabela' | 'form'>('tabela')



  useEffect(obterTodos, [])


  function obterTodos() {
    repo.obterTodos().then(departamentos => {
      setDepartamentos(departamentos)
      setVisivel('tabela')
    })
  }
  function departamentoEditado(departamento: Departamento) {
    setDepartamento(departamento)
    setVisivel('form')
  }
  function departamentoInativado(departamento: Departamento) {

    if (!departamento.inativado) {
      if (confirm('Deseja inativar esse departamento?')) {
        departamento.inativado = true
        console.log(departamento)
      }
    } else {
      if (confirm('Deseja ativar esse departamento?')) {
        departamento.inativado = false
        console.log(departamento)
      } else {

      }
    }
  }


  return (
    <div className={`
    flex h-screen w-full
    bg-gray-200
    `}>
      <MenuLateral></MenuLateral>
      <Layout titulo="Gerenciar Departamento">
        {visivel === 'tabela' ? (
          <>
            <div className='flex justify-end'>
            </div>
            <Tabela departamentos={departamentos}
              departamentoEditado={departamentoEditado}
              departamentoInativado={departamentoInativado}
            />
          </>
        ) :
        <ModalCadastro
        fecharCadastro
        departamento={departamento}
        cadastrarDepartamento
        obterTodos
        />
        }
      </Layout>
    </div>
  )
}
