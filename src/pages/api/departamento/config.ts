import firebase from "firebase/compat/app"
import 'firebase/compat/firestore'
import 'firebase/compat/database'

const firebaseConfig = {
    apiKey: "AIzaSyA2RzMHCpo2BfQmXWeOHO0XONpJHNlcBJE",
    authDomain: "next-pgi-df9dd.firebaseapp.com",
    projectId: "next-pgi-df9dd",
    storageBucket: "next-pgi-df9dd.appspot.com",
    messagingSenderId: "118816817092",
    appId: "1:118816817092:web:5bd4c7bcb6b18345186e91",
    measurementId: "G-YH3K7BVRV3"
  }; 


if(!firebase.apps.length){
    firebase.initializeApp(firebaseConfig)
}else{
    firebase.app()
}
const database = firebase.database()

export {database,firebase}

export default firebase