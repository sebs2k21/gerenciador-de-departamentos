import Departamento from "../../../core/Departamento"
import DepartamentoRepositorio from "../../../core/Repositorio"
import firebase from './config'
import 'firebase/compat/firestore'



export class ColecaoDepartamento implements DepartamentoRepositorio{

    conversor = {
      toFirestore(departamento: Departamento){
        return{
          nome: departamento.name,
          vinculo: departamento.vinculo,
          inativo: departamento.inativado
        }
      },
      fromFirestore(snapshot: firebase.firestore.QueryDocumentSnapshot, options: firebase.firestore.SnapshotOptions): Departamento{
        const dados=snapshot.data(options)
        return new Departamento(dados.nome, dados.vinculo, dados.inativado, snapshot.id)
      }
    }
    async salvar(departamento: Departamento): Promise<Departamento>{
      if(departamento?.id){
        await this.colecao().doc(departamento.id).set(departamento)
        return departamento
      }else{
        const docRef = await this.colecao().add(departamento)
        const doc = await docRef.get()
        return doc.data()
      }
    }
    async excluir(departamento: Departamento): Promise<void> {
      return this.colecao().doc(departamento.id).delete()
    }
    async obterTodos(): Promise<Departamento[]> {
      const query = await this.colecao().get()
      return query.docs.map(doc => doc.data()) ?? []
    }
    async inativar(departamento:Departamento): Promise<any>{
      if (!departamento.inativado) {
        if (confirm) {
          departamento.inativado = true
          console.log(departamento)
        }
      } else {
        if (confirm) {
          departamento.inativado = false
          console.log(departamento)
        } else {
  
        }
    }
    return departamento.inativado
    }
    private colecao(){
      return firebase
      .firestore().collection('departamento')
      .withConverter(this.conversor)
    }
  }