import { v4 as uuid } from 'uuid'

export default class Departamento {
    id: string
    name: string
    vinculo: string
    inativado: boolean

    constructor(name: string, vinculo: string, inativado = true, id:string=null) {
        this.name = name
        this.vinculo = vinculo
        this.inativado = inativado
        this.id = id
    }

    static vazio() {
        return new Departamento('', '',true)
    }
}
