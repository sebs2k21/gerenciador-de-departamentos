import Departamento from "./Departamento";

export default interface DepartamentoRepositorio{
    salvar(departamento: Departamento) : Promise<Departamento>
    excluir(departamento: Departamento) : Promise<void>
    obterTodos(): Promise<Departamento[]>
    inativar(departamento:Departamento): Promise<void>
}